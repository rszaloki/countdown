var AMOUNT = 30*60*1000;

var startDate = (new Date()).getTime();

if(window['require']) {
  var gui = require('nw.gui');
  if(gui.App.argv.length > 0) {
    AMOUNT = gui.App.argv[0] * 1000;
  }
}


requestAnimationFrame(tick);

function pad(value, width) {
  var svalue = value.toString();
  while(svalue.length < width ) {
    svalue = "0"+svalue;
  }
  return svalue;
}

function tick(){
  var currentDate = (new Date()).getTime();
  var current = AMOUNT - (currentDate - startDate);
  var ms = 0, sec = 0, min = 0, hour = 0;
  newFrame = false;
  if(current > 0) {
    document.body.className="positive";
    ms = Math.abs(current % 1000);
    current = Math.floor(current/1000);
    sec = Math.abs(current % 60);
    current = Math.floor(current/60);
    min = Math.abs(current % 60);
    current = Math.floor(current/60);
    hour = current % 60;
    newFrame = true;

  } else {
    document.body.className="negative";
  }
  document.body.innerHTML = '<span class="two">'+pad(hour,2)+'</span>:'+
                            '<span class="two">'+pad(min,2)+'</span>:'+
                            '<span class="two">'+pad(sec,2)+'</span>:'+
                            '<span class="wide">'+pad(ms,4)+'</span>';

  if(newFrame) requestAnimationFrame(tick);
}