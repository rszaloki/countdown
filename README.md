# Countdown

Simple countdown from a given amount down to zero.


## Install

Currently the nodewebkit module isn't installing, so just download and [install](https://github.com/rogerwang/node-webkit#downloads) nodewebkit runtime

Then run it from the project root:

    # alias to nw on OSX
    alias nw="/Applications/node-webkit.app/Contents/MacOS/node-webkit"
    nw ./

[more info](https://github.com/rogerwang/node-webkit/wiki/How-to-run-apps)
